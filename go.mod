module gitlab.com/elixxir/xxdk-wasm

go 1.19

require (
	github.com/armon/circbuf v0.0.0-20190214190532-5111143e8da2
	github.com/hack-pad/go-indexeddb v0.2.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/jwalterweatherman v1.1.0
	gitlab.com/elixxir/client/v4 v4.3.12-0.20230119173451-6b208ab850d4
	gitlab.com/elixxir/crypto v0.0.7-0.20230118183616-f65fc893a7ee
	gitlab.com/elixxir/primitives v0.0.3-0.20230109222259-f62b2a90b62c
	gitlab.com/xx_network/crypto v0.0.5-0.20230113190331-06f2eb12b97f
	gitlab.com/xx_network/primitives v0.0.4-0.20221219230308-4b5550a9247d
	golang.org/x/crypto v0.5.0
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	git.xx.network/elixxir/grpc-web-go-client v0.0.0-20221221204132-2ed1fec765f1 // indirect
	github.com/andres-erbsen/clock v0.0.0-20160526145045-9e14626cd129 // indirect
	github.com/badoux/checkmail v1.2.1 // indirect
	github.com/cenkalti/backoff/v4 v4.1.3 // indirect
	github.com/cloudflare/circl v1.2.0 // indirect
	github.com/desertbit/timer v0.0.0-20180107155436-c41aec40b27f // indirect
	github.com/elliotchance/orderedmap v1.4.0 // indirect
	github.com/forPelevin/gomoji v1.1.8 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/improbable-eng/grpc-web v0.15.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/klauspost/cpuid/v2 v2.1.0 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/oasisprotocol/curve25519-voi v0.0.0-20221003100820-41fad3beba17 // indirect
	github.com/oasisprotocol/deoxysii v0.0.0-20220228165953-2091330c22b7 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/rs/cors v1.8.2 // indirect
	github.com/sethvargo/go-diceware v0.3.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e // indirect
	github.com/soheilhy/cmux v0.1.5 // indirect
	github.com/ttacon/builder v0.0.0-20170518171403-c099f663e1c2 // indirect
	github.com/ttacon/libphonenumber v1.2.1 // indirect
	github.com/tyler-smith/go-bip39 v1.1.0 // indirect
	github.com/zeebo/blake3 v0.2.3 // indirect
	gitlab.com/elixxir/bloomfilter v0.0.0-20211222005329-7d931ceead6f // indirect
	gitlab.com/elixxir/comms v0.0.4-0.20230113232310-712ff1217195 // indirect
	gitlab.com/elixxir/ekv v0.2.1 // indirect
	gitlab.com/xx_network/comms v0.0.4-0.20230113193654-a3a18c6bbb90 // indirect
	gitlab.com/xx_network/ring v0.0.3-0.20220902183151-a7d3b15bc981 // indirect
	gitlab.com/yawning/bsaes.git v0.0.0-20190805113838-0a714cd429ec // indirect
	gitlab.com/yawning/nyquist.git v0.0.0-20221003103146-de5645224a22 // indirect
	gitlab.com/yawning/x448.git v0.0.0-20221003101044-617eb9b7d9b7 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/ratelimit v0.2.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20220822174746-9e6da59bd2fc // indirect
	google.golang.org/grpc v1.49.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
	src.agwa.name/tlshacks v0.0.0-20220518131152-d2c6f4e2b780 // indirect
)
